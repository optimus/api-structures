<?php
function get()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = $input->path[1];
	$input->db = get_user_db($input->path[1]);

	validate('owner', $input->path[1], 'integer', true);

	if (exists($optimus_connection, $input->db, 'associes', 'user', $input->user->id) OR is_admin($input->user->id))
		$authorizations = array('read' => 1, 'write' => 1, 'create' => 1, 'delete' => 1);
	else
	{
		$authorizations = get_rights($input->user->id, $input->owner, 'operations');
		if ($authorizations['read'] == 0)
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder aux recettes");
	}
	
	if (isset($input->id))
	{
		$operation = $optimus_connection->prepare("SELECT * FROM `" . $input->db . "`.compta_operations WHERE id = :id");
		$operation->bindParam(':id', $input->id, PDO::PARAM_INT);
		$operation->execute();
		if ($operation->rowCount() == 0)
			return array("code" => 404, "message" => "Cette operation n'existe pas");
		else
		{
			$operation = $operation->fetchAll(PDO::FETCH_ASSOC);
			return array("code" => 200, "data" => $operation, "authorizations" => $authorizations);
		}
	}
	else if (isset($input->body->fields[0]->name))
	{
		$operations_query = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`compta_operations`");
		include_once 'api_allspark/datagrid.php';
		$results = datagrid_request($optimus_connection, $input->db, 'compta_operations');
		$total = $optimus_connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->results > 0 ? ceil(max($total,1) / $input->body->results) : 1;
		return array("code" => 200, "data" => $results, 'authorizations' => $authorizations, "total" => $total, "last_page" => $last_page);
	}
	else
	{
		$operations =  $optimus_connection->query("SELECT * FROM `" . $input->db . "`.compta_operations");
		return array("code" => 200, "data" => $operations->fetchAll(PDO::FETCH_ASSOC), "authorizations" => $authorizations);
	}
	return array("code" => 400, "message" => "Il n'a été renseigné ni 'identifiant' ni 'champs' dans la requête");
}
?>