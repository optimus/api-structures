<?php
function get()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = $input->path[1];
	$input->db = get_user_db($input->path[1]);

	validate('owner', $input->path[1], 'integer', true);
	
	$input->mutables = array('operation','facture','date','montant');
	validate('id', $input->body->id, 'integer', false);
	validate('operation', $input->body->operation, 'integer', false);
	validate('facture', $input->body->facture, 'integer', false);
	validate('date', $input->body->date, 'date', false);
	validate('montant', $input->body->montant, 'decimal', false);
	
	if (exists($optimus_connection, $input->db, 'associes', 'user', $input->user->id) OR is_admin($input->user->id))
		$authorizations = array('read' => 1, 'write' => 1, 'create' => 1, 'delete' => 1);
	else
	{
		$authorizations = get_rights($input->user->id, $input->owner, 'recettes');
		if ($authorizations['read'] == 0)
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder aux recettes");
	}
	
	if (isset($input->id))
	{
		if (isset($input->body))
		{
			include_once 'api_allspark/datagrid.php';
			$input->fields = validate_fields($optimus_connection, $input->db, 'compta_recettes', array_combine($input->body,$input->body));
			$recette = $optimus_connection->prepare("SELECT " . implode(',', $input->body) . " FROM `" . $input->db . "`.`compta_recettes` WHERE id = :id");
		}
		else
			$recette = $optimus_connection->prepare("SELECT * FROM `" . $input->db . "`.`compta_recettes` WHERE id = :id");
		$recette->bindParam(':id', $input->id, PDO::PARAM_INT);
		$recette->execute();
		if ($recette->rowCount() == 0)
			return array("code" => 404, "message" => "Cette facture n'existe pas");
		else
		{
			$recette = $recette->fetchAll(PDO::FETCH_ASSOC);
			return array("code" => 200, "data" => $recette, "authorizations" => $authorizations);
		}
	}
	else if (isset($input->body))
	{
		include_once 'api_allspark/datagrid.php';
		if (isset($input->body->fields))
		{
			$input->fields = validate_fields($optimus_connection, $input->db, 'compta_recettes', array_combine($input->body->fields,$input->body->fields));
			$query = "SELECT " . implode(',', $input->body->fields) . " FROM `" . $input->db . "`.`compta_recettes` WHERE ";
		}
		else
			$query = "SELECT * FROM `" . $input->db . "`.`compta_recettes` WHERE ";
		
		if (isset($input->body->filters))
			foreach(@$input->body->filters as $filter)
				foreach($filter as $key => $value)
					$query .= $key.'=:'.$key.'  AND  ';
		$query = substr($query,0,-7);

		$recettes = $optimus_connection->prepare($query);
		if (isset($input->body->filters))
			foreach(@$input->body->filters as $filter)
				foreach($filter as $key => $value)
					bind_param($recettes, $key, $value, @$input->fields[$key]);
		
		if($recettes->execute())
			return array("code" => 200, "data" => $recettes->fetchAll(PDO::FETCH_ASSOC), "authorizations" => $authorizations);
		else
			return array("code" => 400, "message" => $recettes->errorInfo()[2]);
	}
	else
	{
		$recettes =  $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`compta_recettes`");
		return array("code" => 200, "data" => $recettes->fetchAll(PDO::FETCH_ASSOC), "authorizations" => $authorizations);
	}
	return array("code" => 400, "message" => "Il n'a été renseigné ni 'identifiant' ni 'champs' dans la requête");
}
?>