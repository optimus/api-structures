<?php
function get()
{
	auth();
	allowed_origins_only();
	$latest = file_get_contents('https://git.cybertron.fr/optimus/api-structures/-/raw/master/VERSION');
	$latest = explode("\n",$latest);
	$current = file_get_contents('/srv/api/api_optimus-structures/VERSION');
	$current = explode("\n",$current);
	$version = array ("name"=>"optimus-structures","current_date"=>intval($current[0]), "current_version"=>$current[1], "latest_date"=>intval($latest[0]), "latest_version"=>$latest[1]);
	return array("code" => 200, "data" => $version);
}


function post()
{
	global $connection, $input, $username, $password;
	auth();
	allowed_origins_only();

	if ($input->path[1] == 'service')
	{
		admin_only();

		$actual_version_date = intval(trim(file('/srv/api/api_optimus-structures/VERSION')[0]));
		$available_version_date = intval(trim(file_get_contents('https://git.cybertron.fr/optimus/api-structures/-/raw/master/VERSION')));

		if ($available_version_date > $actual_version_date)
		{
			exec('rm -R /srv/api/api_optimus-structures');
			exec('mkdir /srv/api/api_optimus-structures');
			exec('git clone https://git.cybertron.fr/optimus/api-structures /srv/api/api_optimus-structures/.');

			$new_version_date = intval(trim(file('/srv/api/api_optimus-structures/VERSION')[0]));

			$structure_sql_files = array_diff(scandir('/srv/api/api_optimus-structures/sql'), array('..', '.'));
			$structures_query = $connection->query("SELECT TABLE_SCHEMA as db FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'factures'");
			while($structure = $structures_query->fetch(PDO::FETCH_ASSOC))
				foreach($structure_sql_files as $structure_sql_file)
					if (preg_replace("/[^0-9]/","",$structure_sql_file) > $actual_version_date)
					{
						$sql = file_get_contents("/srv/api/api_optimus-structures/sql/" . $structure_sql_file);
						$sql = explode(';',$sql);
						$connection->exec("USE `" . $structure['db'] . "`");
						foreach ($sql as $instruction)
							if($instruction!='')
								if (!$connection->query($instruction))
									$errors[] = $connection->errorInfo()[2] . "\n";
					}

			if ($errors)
				return array("code" => 400, "message" => implode("\n",$errors));
			else
				return array("code" => 201, "data" => array("old_version"=>$actual_version_date, "new_version"=>$new_version_date), "message" => "Mise à jour effectuée avec succès");
		}
		else
			return array("code" => 200, "data" => array("actual_version"=>$actual_version_date, "available_version"=>$available_version_date), "message" => "Aucune mise à jour n'est disponible");
	}
	else
	{
		validate('owner', $input->path[1], 'integer', true);
		$input->owner = $input->path[1];
		$input->db = get_user_db($input->path[1]);

		if ($input->owner != $input->user->id AND !is_admin($input->user->id))
			return array("code" => 401, "message" => "Seul l'utilisateur lui même ou un administrateur peut installer un service");

		$connection->query("CREATE DATABASE IF NOT EXISTS `" . $input->db . "` CHARACTER SET utf8 COLLATE utf8_general_ci");
		$connection->query("USE `" . $input->db . "`");
		$sql_files = array_diff(scandir('/srv/api/api_optimus-structures/sql'), array('..', '.'));
		foreach($sql_files as $sql_file)
		{
			$sql = file_get_contents("/srv/api/api_optimus-structures/sql/" . $sql_file);
			$sql = explode(';',$sql);
			foreach ($sql as $instruction)
				if($instruction!='')
					if (!$connection->query($instruction))
						$errors[] = $connection->errorInfo()[2] . "\n";
		}

		exec('mkdir -p /srv/files/' . $input->db . '/==MODELES==/Factures');
		exec('cp -n -R /srv/api/api_optimus-structures/templates/* /srv/files/' . $input->db . '/==MODELES==/');

		$connection->query("CREATE DATABASE IF NOT EXISTS `" . $input->db . "` CHARACTER SET utf8 COLLATE utf8_general_ci");
		$connection->query("USE `" . $input->db . "`");
		$connection->query("REPLACE INTO `server`.`users_services` SET user = '" . $input->owner . "', service = 'optimus-structures'");
		$connection->query("GRANT SELECT, INSERT, UPDATE, DELETE ON `" . $input->db . "`.* TO '" . $username . "'@'localhost' IDENTIFIED BY '" . $password . "'");
		$connection->query("FLUSH PRIVILEGES");

		$cabinet_individuel = $connection->prepare("SELECT * FROM information_schema.tables WHERE table_schema = '" . $input->db . "' AND table_name = 'dossiers_interventions_diligences'");
		$cabinet_individuel->execute();
		if ($cabinet_individuel->rowCount() > 0)
		{
			$connection->query("INSERT INTO `" . $input->db . "`.`associes` SET server='" . $input->server . "', user = '" . $input->owner . "', entree = '2000-01-01'");
			$connection->query("INSERT INTO `" . $input->db . "`.`structures` SET server='" . $input->server . "', user = '" . $input->owner . "', entree = '2000-01-01'");
		}
		if (sizeof(@$errors)>0)
			return array("code" => 400, "message" => $errors, "data" => array("version_date" => $version[0], "version" => $version[1]));
		else
			return array("code" => 201, "data" => array("version_date" => trim(file('/srv/api/api_optimus-structures/VERSION')[0]), "version" => trim(file('/srv/api/api_optimus-structures/VERSION')[1])));
	}
}


function delete()
{
	global $connection, $input, $username, $password;
	auth();
	allowed_origins_only();

	validate('owner', $input->path[1], 'integer', true);
		$input->owner = $input->path[1];
		$input->db = get_user_db($input->path[1]);

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul l'utilisateur lui même ou un administrateur peut désinstaller un service");
	
	$associes = $connection->prepare("SELECT * FROM  `" . $input->db . "`.`associes` WHERE id != '" . $input->owner . "'");
	$associes->execute();
	if ($associes->rowCount() > 0)
		return array("code" => 401, "message" => "Cette structure ne peut pas être supprimée car elle contient des associés");
	
	$factures = $connection->prepare("SELECT * FROM  `" . $input->db . "`.`factures`");
	$factures->execute();
	if ($factures->rowCount() > 0)
		return array("code" => 401, "message" => "Cette structure ne peut pas être supprimée car elle a émis des factures");

	$sql = <<<EOD
	DROP TABLE `$input->db`.associes;
	DROP TABLE `$input->db`.bilans;
	DROP TABLE `$input->db`.compta_affectations;
	DROP TABLE `$input->db`.compta_comptes;
	DROP TABLE `$input->db`.compta_depenses;
	DROP TABLE `$input->db`.compta_operations;
	DROP TABLE `$input->db`.compta_plan;
	DROP TABLE `$input->db`.compta_recettes;
	DROP TABLE `$input->db`.compta_regles;
	DROP TABLE `$input->db`.factures;
	EOD;

	$sql = explode(';',$sql);
	foreach ($sql as $instruction)
		if($instruction!='')
			if (!$connection->query($instruction))
				$errors[] = $connection->errorInfo()[2] . "\n";

	$connection->query("DELETE FROM `server`.`users_services` WHERE user = '" . $input->owner . "' AND service = 'optimus-structures'");
	$connection->query("REVOKE SELECT, INSERT, UPDATE, DELETE ON `" . $input->db . "`.* TO '" . $username . "'@'localhost' IDENTIFIED BY '" . $password . "'");
	$connection->query("FLUSH PRIVILEGES");

	$cabinet_individuel = $connection->prepare("SELECT * FROM information_schema.tables WHERE table_schema = '" . $input->db . "' AND table_name = 'dossiers_interventions_diligences'");
	$cabinet_individuel->execute();
	if ($cabinet_individuel->rowCount() > 0)
		$connection->query("DELETE FROM `" . $input->db . "`.`structures` WHERE server='" . $input->server . "' AND user = '" . $input->owner . "'");
	
	if ($errors)
		return array("code" => 400, "message" => $errors);
	else
		return array("code" => 200);
}
?>