<?php
function get()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = $input->path[1];
	$input->db = get_user_db($input->path[1]);
	validate('owner', $input->owner, 'integer', true);

	$associes = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`associes`")->fetchAll(PDO::FETCH_ASSOC);
	return array("code" => 200, "data" => $associes);
}


function post()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = $input->path[1];
	$input->db = get_user_db($input->path[1]);
	validate('owner', $input->owner, 'integer', true);

	$input->mutables = array('user','entree','sortie');
	validate('user', $input->body->user, 'integer', true);
	validate('entree', $input->body->entree, 'date', false);
	validate('sortie', $input->body->sortie, 'date', false);
	
	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou la structure elle même peut ajouter un associé");
	else
		$authorizations = array('read' => 1, 'write' => 1, 'create' => 1, 'delete' => 1);
	
	if(exists($optimus_connection, $input->db, 'associes', 'id', $input->body->user))
		return array("code" => 409, "message" => "Cet utilisateur fait déjà partie des membres de la structure");
	
	$query = "INSERT INTO `" . $input->db . "`.`associes` SET ";
	foreach($input->body as $key => $value)
		if (in_array($key, $input->mutables))
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);

	$associe = $optimus_connection->prepare($query);
	foreach($input->body as $key => $value)
		if (in_array($key, $input->mutables))
			$associe->bindParam(':'. $key, $input->body->$key, @$input->fields[$key]);
	
	if($associe->execute())
	{
		$new_id = $optimus_connection->lastInsertId();
		$new_associe = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`associes` WHERE id = " . $new_id)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 201, "data" => $new_associe, "authorizations" => $authorizations);
	}
	else
		return array("code" => 400, "message" => $associe->errorInfo()[2]);
}


function patch()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = $input->path[1];
	$input->db = get_user_db($input->path[1]);
	$input->id = $input->path[3];
	validate('owner', $input->owner, 'integer', true);
	validate('id', $input->id, 'integer', true);

	$input->mutables = array('user','entree','sortie');
	validate('user', $input->body->user, 'integer', false);
	validate('entree', $input->body->entree, 'date', false);
	validate('sortie', $input->body->sortie, 'date', false);
	
	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou la structure elle même peut modifier un associé");
	else
		$authorizations = array('read' => 1, 'write' => 1, 'create' => 1, 'delete' => 1);

	if(!exists($optimus_connection, $input->db, 'associes', 'id', $input->id))
		return array("code" => 409, "message" => "Cet associé n'existe pas");
	
	$query = "UPDATE `" . $input->db . "`.`associes` SET ";
	foreach($input->body as $key => $value)
		if (in_array($key, $input->mutables))
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);
	$query .= " WHERE id = '" . $input->id . "'";

	$associe = $optimus_connection->prepare($query);
	foreach($input->body as $key => $value)
		if (in_array($key, $input->mutables))
			$associe->bindParam(':'. $key, $input->body->$key, @$input->fields[$key]);
	
	if($associe->execute())
		return array("code" => 200, "authorizations" => $authorizations);
	else
		return array("code" => 400, "message" => $associe->errorInfo()[2]);
}


function delete()
{
	global $optimus_connection, $input, $domain;
	auth();
	allowed_origins_only();

	$input->owner = $input->path[1];
	$input->db = get_user_db($input->path[1]);
	$input->id = $input->path[3];
	validate('owner', $input->owner, 'integer', true);
	validate('id', $input->id, 'integer', true);
	
	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou la structure elle même peut supprimer un associé");

	$associe = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`associes` WHERE id = " . $input->id)->fetch(PDO::FETCH_ASSOC);
	
	$exists = $optimus_connection->prepare("SELECT id FROM `" . $input->db . "`.`factures` WHERE server='" . $domain . "' AND owner = '" . $associe['user'] . "'");
	$exists->execute();
	if ($exists->rowCount() > 0)
		return array("code" => 409, "message" => "Cet associé ne peut pas être supprimé car il a créé des factures dans la structure " . $database['table_schema']);

	$delete = $optimus_connection->query("DELETE FROM `" . $input->db . "`.`associes` WHERE id = '" . $input->id . "'");
	return array("code" => 200);
}
?>