<?php
function get()
{
	global $connection, $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = $input->path[1];
	$input->db = get_user_db($input->path[1]);
	$input->id = $input->path[3];

	validate('owner', $input->path[1], 'integer', true);
	validate('id', $input->path[3], 'integer', false);

	if (exists($optimus_connection, $input->db, 'associes', 'user', $input->user->id) OR is_admin($input->user->id))
		$authorizations = array('read' => 1, 'write' => 1, 'create' => 1, 'delete' => 1);
	else
	{
		if (@$input->id)
			$authorizations = get_rights($input->user->id, $input->owner, 'factures/'.@$input->id);
		else
			$authorizations = get_rights($input->user->id, $input->owner, 'factures');
		if ($authorizations['read'] == 0)
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour accéder à cette facture");
	}
	
	//EXPORT PDF OU ODT D'UNE FACTURE DETERMINEE
	if ((@$input->body->format == 'odt' OR @$input->body->format == 'pdf') AND isset($input->id))
	{
		$facture_uid = substr(sha1(mt_rand()),0,16);
		include_once 'api_optimus-structures/facture_generator.php';
		if ($input->body->detail == '1')
		{
			$intervention_uid = substr(sha1(mt_rand()),0,16);
			$facture = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`factures` WHERE id = '" . $input->id . "'")->fetch(PDO::FETCH_ASSOC);
			$input->db = get_user_db($facture['owner']);
			$input->subid = $facture['intervention'];
			include_once 'api_optimus-avocats/intervention_generator.php';
			exec("ooo_cat '/srv/api/tmp/" . $facture_uid . ".odt' '/srv/api/tmp/" . $intervention_uid . ".odt' -o '/srv/api/tmp/" . $facture_uid . "_detail.odt'", $output);
		}
		if ($input->body->format == 'pdf')
			exec("LC_ALL=fr_FR.UTF-8 export HOME=/srv/api/tmp && libreoffice --headless --convert-to pdf '/srv/api/tmp/" . $facture_uid . ($input->body->detail == '1' ? '_detail' : '') . ".odt' --outdir '/srv/api/tmp/' 2>&1", $output);
		$file_content = base64_encode(file_get_contents('/srv/api/tmp/' . $facture_uid . ($input->body->detail == '1' ? '_detail' : '') . '.' . $input->body->format));
		exec ('rm -R /srv/api/tmp/'.$facture_uid);
		exec ('rm -R /srv/api/tmp/'.$facture_uid.'.*');
		return array("code" => 200, "data" => $file_content, "authorizations" => $authorizations);
	}
	//REQUETE SUR UNE FACTURE IDENTIFIÉE
	else if (isset($input->id))
	{
		if (isset($input->body))
		{
			include_once 'api_allspark/datagrid.php';
			$input->fields = validate_fields($optimus_connection, $input->db, 'factures', array_combine($input->body,$input->body));
			$facture = $optimus_connection->prepare("SELECT " . implode(',', $input->body) . " FROM `" . $input->db . "`.`factures` WHERE id = :id");
		}
		else
			$facture = $optimus_connection->prepare("SELECT * FROM `" . $input->db . "`.`factures` WHERE id = :id");
		$facture->bindParam(':id', $input->id, PDO::PARAM_INT);
		$facture->execute();
		if ($facture->rowCount() == 0)
			return array("code" => 404, "message" => "Cette facture n'existe pas");
		else
		{
			$facture = $facture->fetchAll(PDO::FETCH_ASSOC);
			$facture['db'] = $input->db;
			return array("code" => 200, "data" => $facture, "authorizations" => $authorizations);
		}
	}
	//REQUETE SUR TOUTES LES FACTURES AU FORMAT DATAGRID
	else if (isset($input->body->fields[0]->name))
	{
		$factures_query = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`factures`");
		while($facture = $factures_query->fetch(PDO::FETCH_ASSOC))
		{
			if ($facture['server'] == substr($_SERVER['HTTP_HOST'],4) AND !array_key_exists($facture['owner'],$user_databases))
				$user_databases[$facture['owner']] = get_user_db($facture['owner']);
				
			if ($facture['server'] == substr($_SERVER['HTTP_HOST'],4))
			{
				$client = $optimus_connection->query("SELECT id, COALESCE(NULLIF(IF(type=30,UPPER(company_name),TRIM(CONCAT(UPPER(lastname),' ',firstname))),''),'????????') as value FROM `" . $user_databases[$facture['owner']] . "`.contacts WHERE id = " . $facture['client'])->fetch(PDO::FETCH_ASSOC);
				$input->dblink->clients[$client['id']] = $client['value'];
				
				$dossier = $optimus_connection->query("SELECT id, nom FROM `" . $user_databases[$facture['owner']] . "`.dossiers WHERE id = " . $facture['dossier'])->fetch(PDO::FETCH_ASSOC);
				$input->dblink->dossiers[$dossier['id']] = $dossier['nom'];
			}
			$payes = $optimus_connection->query("SELECT SUM(montant) as montant FROM `" . $input->db . "`.compta_recettes WHERE facture = " . $facture['id'])->fetch(PDO::FETCH_ASSOC);
			$input->dblink->payes[$facture['id']] = $payes['montant'];
			$input->dblink->restes[$facture['id']] = $facture['total'] - $payes['montant'];
			$input->dblink->ratios[$facture['id']] = $facture['total']==0 ? 0 :round($payes['montant'] / $facture['total'] * 100,2);
		}

		include_once 'api_allspark/datagrid.php';
		$results = datagrid_request($optimus_connection, $input->db, 'factures');
		$total = $optimus_connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->results > 0 ? ceil(max($total,1) / $input->body->results) : 1;
		return array("code" => 200, "data" => $results, 'authorizations' => $authorizations, "total" => $total, "last_page" => $last_page);
	}
	//REQUETE SUR TOUTES LES FACTURES AVEC UNE LISTE DE CHAMPS DETERMINÉS
	else if (isset($input->body))
	{
		include_once 'api_allspark/datagrid.php';
		if (isset($input->body->fields))
		{
			$input->fields = validate_fields($optimus_connection, $input->db, 'factures', array_combine($input->body->fields,$input->body->fields));
			$query = "SELECT " . implode(',', $input->body->fields) . " FROM `" . $input->db . "`.`factures` WHERE ";
		}
		else
			$query = "SELECT * FROM `" . $input->db . "`.`factures` WHERE ";
		
		if (isset($input->body->filters))
			foreach(@$input->body->filters as $filter)
				foreach($filter as $key => $value)
					$query .= $key.'=:'.$key.'  AND  ';
		$query = substr($query,0,-7);

		$factures = $optimus_connection->prepare($query);
		if (isset($input->body->filters))
			foreach(@$input->body->filters as $filter)
				foreach($filter as $key => $value)
					bind_param($factures, $key, $value, @$input->fields[$key]);
		
		if($factures->execute())
		{
			$data = $factures->fetchAll(PDO::FETCH_ASSOC);
			$total = sizeof($data);
			$last_page = $input->body->results > 0 ? ceil(max($total,1) / $input->body->results) : 1;
			if (is_array($data) AND $input->body->page AND $input->body->results)
				$data = array_slice($data,($input->body->page-1)*$input->body->results,$input->body->results);
			return array("code" => 200, "data" => $data, "authorizations" => $authorizations, "total" => $total, "last_page" => $last_page);
		}
		else
			return array("code" => 400, "message" => $factures->errorInfo()[2]);
	}
	else
	{
		$factures =  $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`factures`");
		return array("code" => 200, "data" => $factures->fetchAll(PDO::FETCH_ASSOC), "authorizations" => $authorizations);
	}
	return array("code" => 400, "message" => "Il n'a été renseigné ni 'identifiant' ni 'champs' dans la requête");
}

function post()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = $input->path[1];
	$input->db = get_user_db($input->owner);

	$input->mutables = array('numero','server','owner','client','dossier','intervention','date','template','tva','provision','reminder1','reminder2','reminder3','reminder4','reminder5','notes','irrecouvrable','total');
	validate('numero', $input->body->numero, 'string', false);
	validate('server', $input->body->server, 'domain', true);
	validate('owner', $input->body->owner, 'integer', true);
	validate('client', $input->body->client, 'integer', false);
	validate('dossier', $input->body->dossier, 'integer', false);
	validate('intervention', $input->body->intervention, 'integer', false);
	validate('date', $input->body->date, 'date', false);
	validate('template', $input->body->template, 'string', false);
	validate('tva', $input->body->tva, 'integer', false);
	validate('provision', $input->body->provision, 'boolean', false);
	validate('reminder1', $input->body->reminder1, 'date', false);
	validate('reminder2', $input->body->reminder2, 'date', false);
	validate('reminder3', $input->body->reminder3, 'date', false);
	validate('reminder4', $input->body->reminder4, 'date', false);
	validate('reminder5', $input->body->reminder5, 'date', false);
	validate('notes', $input->body->notes, 'text', false);
	validate('irrecouvrable', $input->body->irrecouvrable, 'boolean', false);
	validate('total', $input->body->total, 'decimal', false);
	
	if (exists($optimus_connection, $input->db, 'associes', 'user', $input->user->id) OR is_admin($input->user->id))
		$authorizations = array('read' => 1, 'write' => 1, 'create' => 1, 'delete' => 1);
	else
	{
		$authorizations = get_rights($input->user->id, $input->owner, 'factures');
		if ($authorizations['create'] == 0)
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour créer des factures");
	}

	if (!isset($input->body->numero))
	{
		$last_numero = $optimus_connection->query("SELECT numero FROM `" . $input->db . "`.`factures` WHERE numero LIKE '" . date('y') . "-%' ORDER BY id DESC LIMIT 1")->fetchObject();
		$input->body->numero = date('y') . '-' . str_pad(intval(substr($last_numero->numero,3))+1, 5, "0", STR_PAD_LEFT);
	}
	if (!isset($input->body->date))
		$input->body->date = date('Y-m-d');
	
	if (exists($optimus_connection, $input->db, 'factures', 'numero', $input->body->numero))
		return array("code" => 409, "message" => "Une facture portant ce numéro existe déjà dans la base");
	
	$already_facture = $optimus_connection->prepare("SELECT id FROM `" . $input->db . "`.`factures` WHERE server = :server AND owner = :owner AND intervention = :intervention");
	$already_facture->bindParam(':server', $input->body->server);
	$already_facture->bindParam(':owner', $input->body->owner);
	$already_facture->bindParam(':intervention', $input->body->intervention);
	$already_facture->execute();
	if ($already_facture->rowCount() > 0)
		return array("code" => 409, "message" => "Cette fiche d'intervention a déjà été facturée");
	
		$query = "INSERT INTO `" . $input->db . "`.`factures` SET ";
	foreach($input->body as $key => $value)
		if (in_array($key, $input->mutables))
			$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);

	$facture = $optimus_connection->prepare($query);
	foreach($input->body as $key => $value)
		if (in_array($key, $input->mutables))
			$facture->bindParam(':'. $key, $input->body->$key, $input->fields[$key]);

	if($facture->execute())
	{
		$new_id = $optimus_connection->lastInsertId();
		$new_facture = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.`factures` WHERE id = " . $new_id)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 201, "data" => $new_facture, "authorizations" => $authorizations);
	}
	else
		return array("code" => 400, "message" => $facture->errorInfo()[2]);
}


function patch()
{
	global $optimus_connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = $input->path[1];
	$input->db = get_user_db($input->path[1]);
	$input->id = $input->path[3];

	validate('owner', $input->path[1], 'integer', true);
	validate('id', $input->path[3], 'integer', true);

	$input->mutables = array('numero','server','owner','client','dossier','intervention','date','template','tva','provision','reminder1','reminder2','reminder3','reminder4','reminder5','notes','irrecouvrable','total');
	if (!isset($input->body)) return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");
	validate('numero', $input->body->numero, 'locked', false);
	validate('server', $input->body->server, 'locked', false);
	validate('owner', $input->body->owner, 'locked', false);
	validate('client', $input->body->client, 'integer', false);
	validate('dossier', $input->body->dossier, 'integer', false);
	validate('intervention', $input->body->intervention, 'integer', false);
	validate('date', $input->body->date, 'date', false);
	validate('template', $input->body->template, 'string', false);
	validate('tva', $input->body->tva, 'integer', false);
	validate('provision', $input->body->provision, 'boolean', false);
	validate('reminder1', $input->body->reminder1, 'date', false);
	validate('reminder2', $input->body->reminder2, 'date', false);
	validate('reminder3', $input->body->reminder3, 'date', false);
	validate('reminder4', $input->body->reminder4, 'date', false);
	validate('reminder5', $input->body->reminder5, 'date', false);
	validate('notes', $input->body->notes, 'text', false);
	validate('irrecouvrable', $input->body->irrecouvrable, 'boolean', false);
	validate('total', $input->body->total, 'decimal', false);

	if (exists($optimus_connection, $input->db, 'associes', 'user', $input->user->id) OR is_admin($input->user->id))
		$authorizations = array('read' => 1, 'write' => 1, 'create' => 1, 'delete' => 1);
	else
	{
		if (@$input->id)
			$authorizations = get_rights($input->user->id, $input->owner, 'factures/'.@$input->id);
		else
			$authorizations = get_rights($input->user->id, $input->owner, 'factures');
		if ($authorizations['write'] == 0)
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier cette facture");
	}

	if (!exists($optimus_connection, $input->db, 'factures', 'id', $input->id))
		return array("code" => 404, "message" => "Cette facture n'existe pas");
	
	if (exists($optimus_connection, $input->db, 'compta_recettes', 'facture', $input->id))
		if(isset($input->body->client) OR isset($input->body->dossier) OR isset($input->body->intervention) OR isset($input->body->date) OR isset($input->body->tva))
			return array("code" => 403, "message" => "Cette facture ne peut pas être modifiée car des paiements ont déjà été comptabilisés");
	
	$query = "UPDATE `" . $input->db . "`.`factures` SET ";
	foreach($input->body as $key => $value)
	if (in_array($key, $input->mutables))
		$query .= $key.'=:'.$key.',';
	$query = substr($query,0,-1);
	$query .= " WHERE id = '" . $input->id . "'";

	$facture = $optimus_connection->prepare($query);
	foreach($input->body as $key => $value)
		if (in_array($key, $input->mutables))
			$facture->bindParam(':'. $key, $input->body->$key, $input->fields[$key]);
	
	if($facture->execute())
		return array("code" => 200, "authorizations" => $authorizations);
	else
		return array("code" => 400, "message" => $facture->errorInfo()[2]);
}

function delete()
{
	return array("code" => 403, "message" => "conformément aux règles légales, une facture ne peut pas être supprimée.\nElle ne peut qu'être annulée par un avoir.");
}
?>