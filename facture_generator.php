<?php
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) { die('Access denied'); };

$facture = $optimus_connection->query("SELECT * FROM `" . $input->db . "`.factures WHERE id = '" . $input->id . "'")->fetch(PDO::FETCH_ASSOC);
$owner = $connection->query("SELECT email FROM `server`.`users` WHERE id = '" . $facture['owner'] . "'")->fetch(PDO::FETCH_ASSOC);
$intervention = $optimus_connection->query("SELECT * FROM `" . $owner['email'] . "`.dossiers_interventions WHERE id = '" . $facture['intervention'] . "'")->fetch(PDO::FETCH_ASSOC);
$dossier = $optimus_connection->query("SELECT * FROM `" . $owner['email'] . "`.dossiers WHERE id = '" . $facture['dossier'] . "'")->fetch(PDO::FETCH_ASSOC);
$client = $optimus_connection->query("SELECT * FROM `" . $owner['email'] . "`.contacts WHERE id = '" . $facture['client'] . "'")->fetch(PDO::FETCH_ASSOC);

if ($client)
{
	$pays = file_get_contents('https://api.optimus-avocats.fr/constants?data={"db":"pays"}');
	$pays = json_decode($pays,true);
	$pays_id = array_search($client['country'], array_column($pays['data'], 'id'));
	$pays = $pays['data'][$pays_id];
}

$tva_rate = file_get_contents('https://api.optimus-avocats.fr/constants?data={"db":"tva_rates"}');
$tva_rate = json_decode($tva_rate,true);
$tva_rate_id = array_search($facture['tva'], array_column($tva_rate['data'], 'id'));
$tva_rate = $tva_rate['data'][$tva_rate_id];

$odt = new ZipArchive;
if ($odt->open('/srv/files/' .  $input->db . '/==MODELES==/Factures/' . $facture['template'] . '.odt') === TRUE) 
{
	$odt->extractTo('/srv/api/tmp/'.$facture_uid);
	$odt->close();
	$odt_content = file_get_contents('/srv/api/tmp/'.$facture_uid.'/content.xml');
	
	if ($client['type']==30)
		$odt_content = str_replace('CLIENT_NOM',strtoupper($client['company_name']),$odt_content);
	else
		$odt_content = str_replace('CLIENT_NOM',$client['firstname'] . " " . strtoupper($client['lastname']),$odt_content);

	$odt_content = str_replace('CLIENT_ADRESSE',str_replace("\n","<text:line-break/>",$client['address']),$odt_content);
		
	if (preg_match('/cedex/i',$client['address']) > 0)
	{
		$odt_content = str_replace('CLIENT_CP','',$odt_content);
		$odt_content = str_replace('CLIENT_VILLE','',$odt_content);
	}
	else
	{
		$odt_content = str_replace('CLIENT_CP',$client['zipcode'],$odt_content);
		$odt_content = str_replace('CLIENT_VILLE',strtoupper($client['city_name']),$odt_content);
		$odt_content = str_replace('CLIENT_PAYS',strtoupper($pays['value']),$odt_content);
	}

	$odt_content = str_replace('DOSSIER_NOM',$dossier['nom'],$odt_content);
	$odt_content = str_replace('DOSSIER_NUMERO',$dossier['numero'],$odt_content);
	$odt_content = str_replace('DATE_FACTURE',date('d/m/Y',strtotime($facture['date'])),$odt_content);

	if ($client['tva']!='')
		$odt_content = str_replace('TVA_INTRACOM',$client['tva'],$odt_content);
	else
		$odt_content = str_replace('TVA_INTRACOM','',$odt_content);

	if ($client['siret']!='')
		$odt_content = str_replace('CLIENT_SIRET',$client['siret'],$odt_content);
	else
		$odt_content = str_replace('CLIENT_SIRET','',$odt_content);
	
	if ($facture['total'] >= 0)
	{
		$odt_content = str_replace('FACTURE_TYPE','FACTURE',$odt_content);
		$odt_content = str_replace('INVOICE_TYPE','INVOICE',$odt_content);
		$odt_content = str_replace('RECHNUNG_TYP','RECHNUNG',$odt_content);
	}
	else
	{
		$odt_content = str_replace('FACTURE_TYPE','AVOIR',$odt_content);
		$odt_content = str_replace('INVOICE_TYPE','CREDIT NOTE',$odt_content);
		$odt_content = str_replace('RECHNUNG_TYP','GUTSCHRIFT',$odt_content);
	}
		
	
	$odt_content = str_replace('FACTURE_NUMERO',$facture['numero'],$odt_content);

	$odt_content = str_replace('TOTAL_HONORAIRES',number_format($intervention['honoraires'],2,',',' '),$odt_content);
	$odt_content = str_replace('TOTAL_FRAIS',number_format($intervention['frais'],2,',',' '),$odt_content);
	$odt_content = str_replace('TOTAL_HT',number_format($intervention['honoraires']+$intervention['frais'],2,',',' '),$odt_content);
	$odt_content = str_replace('TOTAL_TVA',number_format(($intervention['honoraires']+$intervention['frais'])*$tva_rate['value']/100,2,',',' '),$odt_content);
	$odt_content = str_replace('TOTAL_DEBOURS',number_format($intervention['debours'],2,',',' '),$odt_content);
	$odt_content = str_replace('TOTAL_TTC',number_format($facture['total'],2,',',' '),$odt_content);

	if ($facture['tva']==1 or $facture['tva']==2 or $facture['tva']==6)
	{
		$odt_content = str_replace('TAUX_TVA',$tva_rate['value'] . ' %',$odt_content);
		$odt_content = str_replace('VAT_RATE',$tva_rate['value'] . ' %',$odt_content);
		$odt_content = str_replace('MWST_PROZENT',$tva_rate['value'] . ' %',$odt_content);
	}	
	else if ($facture['tva']==3 OR $facture['tva']==4)
	{
		$odt_content = str_replace('TAUX_TVA','Non applicable - ART. 259B CGI',$odt_content);
		$odt_content = str_replace('VAT_RATE','Not applicable - ART. 259B CGI',$odt_content);
		$odt_content = str_replace('MWST_PROZENT','Mehrwertsteuerfrei - Artikel 259B CGI',$odt_content);
	}
	else if ($facture['tva']==5)
	{
		$odt_content = str_replace('TAUX_TVA','Non applicable - ART. 293B CGI',$odt_content);
		$odt_content = str_replace('VAT_RATE','Not applicable - ART. 293B CGI',$odt_content);
		$odt_content = str_replace('MWST_PROZENT','Mehrwertsteuerfrei - Artikel 259B CGI',$odt_content);
	}

	if (@$input->body->detail==1)
	{
		$odt_content = str_replace('DETAILED_TIMESHEET_AT_BACK','See detailed time sheet at the back',$odt_content);
		$odt_content = str_replace('DETAIL_DES_DILIGENCES_AU_VERSO','Détail des diligences au verso',$odt_content);
		$odt_content = str_replace('DETAILLIERTE_ZUSTAND_AUF_DER_RUCKZEITE','Detaillierte Zustand auf der Rückseite',$odt_content);
	}
	else
	{
		$odt_content = str_replace('DETAILED_TIMESHEET_AT_BACK','',$odt_content);
		$odt_content = str_replace('DETAIL_DES_DILIGENCES_AU_VERSO','',$odt_content);
		$odt_content = str_replace('DETAILLIERTE_ZUSTAND_AUF_DER_RUCKZEITE','',$odt_content);
	}
	
	file_put_contents('/srv/api/tmp/'.$facture_uid.'/content.xml',$odt_content);
	$zip = new ZipArchive();
	$zip->open('/srv/api/tmp/'.$facture_uid.'.odt', ZipArchive::CREATE | ZipArchive::OVERWRITE);
	$files = new RecursiveIteratorIterator (new RecursiveDirectoryIterator('/srv/api/tmp/'.$facture_uid), RecursiveIteratorIterator::LEAVES_ONLY);
	foreach ($files as $name => $file)
		if (!$file->isDir())
		{
			$filePath = $file->getRealPath();
			$relativePath = substr($filePath, strlen('/srv/api/tmp/'.$facture_uid) + 1);
			$zip->addFile($filePath, $relativePath);
		}
	if (!$zip->close())
		return array("code" => 400, "message" => "Une erreur est survenue");
}
?>
