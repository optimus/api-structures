REPLACE INTO `server`.`modules_groups` SET name="optimus-structures", displayname="Structure", position=31;

REPLACE INTO `server`.`modules` SET name="factures", displayname="Factures", `module`="optimus-structures", `group`="optimus-structures", position=1, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=0, description=null;
REPLACE INTO `server`.`modules` SET name="grand-livre", displayname="Grand Livre", `module`="optimus-structures", `group`="optimus-structures", position=2, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="releves", displayname="Relevés", `module`="optimus-structures", `group`="optimus-structures", position=3, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="des", displayname="DES", `module`="optimus-structures", `group`="optimus-structures", position=4, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="tva", displayname="TVA", `module`="optimus-structures", `group`="optimus-structures", position=5, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="recettes", displayname="Recettes", `module`="optimus-structures", `group`="optimus-structures", position=6, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="stats", displayname="Statistiques", `module`="optimus-structures", `group`="optimus-structures", position=7, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="benefices", displayname="Bénéfices", `module`="optimus-structures", `group`="optimus-structures", position=8, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="paie", displayname="Paie", `module`="optimus-structures", `group`="optimus-structures", position=9, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="salaries", displayname="Salariés", `module`="optimus-structures", `group`="optimus-structures", position=10, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;
REPLACE INTO `server`.`modules` SET name="charges-perso", displayname="Charges Perso", `module`="optimus-structures", `group`="optimus-structures", position=11, status=1, install=null, uninstall=null, user_install=null, user_uninstall=null, admin_only=0, disabled=1, description=null;

ALTER TABLE `factures` ADD COLUMN `owner` int unsigned DEFAULT NULL AFTER `server`;
UPDATE `factures` INNER JOIN `server`.`users` ON `users`.email = `factures`.db SET `factures`.owner = `users`.id;
UPDATE `factures` SET `template` = SUBSTRING_INDEX(`template`,'.',1);
ALTER TABLE `factures` DROP COLUMN `db`;
ALTER TABLE `factures` DROP COLUMN `language`;

ALTER TABLE `members` RENAME `associes`;
ALTER TABLE `associes` ADD COLUMN `user` int unsigned DEFAULT NULL AFTER `server`;
ALTER TABLE `associes` ADD COLUMN `entree` date DEFAULT NULL AFTER `user`;
ALTER TABLE `associes` ADD COLUMN `sortie` date DEFAULT NULL AFTER `entree`;
UPDATE `associes` INNER JOIN `server`.`users` ON `users`.email = `associes`.member SET `associes`.user = `users`.id;
ALTER TABLE `associes` DROP COLUMN `member`;